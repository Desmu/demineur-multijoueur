import DemineurRoom from "./js/room.js";
import DemineurGrid from "./js/grid.js";

const configuration = await (await fetch('./configuration.json')).json();
let ws = null;
if (configuration.env === "dev") {
    ws = new WebSocket(`ws://${configuration.serverDomain}:${configuration.serverPort}`);
} else if (configuration.env === "prod") {
    ws = new WebSocket(`wss://${configuration.serverDomain}:${configuration.serverPort}`);
}

ws.onopen = () => {
    document.getElementById("room_join_code").addEventListener("click", () => {
        document.getElementById("room_join").click();
    });
    document.getElementById("submit_player").addEventListener("click", () => {
        DemineurRoom.roomTested(ws);
    });
};

ws.onmessage = (message) => {
    const data = JSON.parse(message.data);
    switch (data.method) {
        case "room_destroyed":
            document.body.innerHTML = "<p>Partie terminée, veuillez recharger la page pour rejouer.</p>";
            break;
        case "room_not_found":
            alert("Erreur : salle non trouvée, veuillez réessayer.");
            break;
        case "no_public_room":
            alert("Aucune salle publique n'est ouverte, vous pouvez en créer une.");
            break;
        case "room_created":
            DemineurRoom.roomCreated(ws, data.roomId, data.roomMode);
            DemineurRoom.formEventsAdmin();
            DemineurRoom.loadStyle();
            break;
        case "room_found":
            DemineurRoom.roomFound(data.roomId, data.roomMode);
            DemineurRoom.loadStyle();
            break;
        case "room_styled":
            DemineurGrid.changeStyle(data.gridStyle);
            break;
        case "players_update":
            DemineurRoom.updateRoomPlayers(data.players, data.gameMode);
            break;
        case "players_update_amount":
            DemineurRoom.updateRoomPlayersAmount(data.playersAmount, data.gameMode);
            break;
        case "wait_next_game":
            DemineurRoom.changeInfos("<p>Pas de manche en cours, vous participerez à la prochaine.</p>");
            break;
        case "wait_next_game_versus":
            DemineurRoom.changeInfos("<p>Manche versus en cours, vous participerez à la prochaine.</p>");
            break;
        case "wait_next_battle_royale":
            DemineurRoom.changeInfos("<p>Battle royale en cours, vous participerez à la prochaine.</p>");
            break;
        case "grid_ready":
            DemineurGrid.createGrid(ws, data.rowsTotal, data.colsTotal, data.bombsTotal, data.gridStyle, data.gameMode, (data.battleRoyalePlayersAmount ? data.battleRoyalePlayersAmount : null), (data.battleRoyaleTimeAllowedBeforeLost ? data.battleRoyaleTimeAllowedBeforeLost : null));
            DemineurRoom.lockStyle();
            break;
        case "grid_time_before_start":
            DemineurRoom.intervalTimeBeforeStart(data.timeBeforeStart);
            break;
        case "grid_time_elapsed":
            document.getElementById("time").innerHTML = DemineurGrid.convertToDigits("time", data.timeElapsed);
            break;
        case "grid_current_state":
            DemineurGrid.createGrid(ws, data.rowsTotal, data.colsTotal, data.bombsTotal, data.gridStyle, data.gameMode, null, null);
            DemineurGrid.clickedGrid(data.cells);
            break;
        case "grid_clicked":
            DemineurGrid.clickedGrid(data.cells);
            break;
        case "grid_show_bombs":
            DemineurGrid.clickedGrid(data.cells);
            break;
        case "grid_flagged":
            DemineurGrid.flaggedGrid(data.row, data.col, data.type, data.flags);
            break;
        case "grid_lost":
            DemineurGrid.state = "lost";
            if (data.gameMode === "battle_royale") {
                DemineurRoom.changeInfos("<p>Vous avez perdu.</p>");
            }
            DemineurGrid.changeSmiley("ko");
            DemineurRoom.updateRoomPlayers(data.players, data.gameMode);
            DemineurGrid.finishedGrid();
            break;
        case "grid_won":
            DemineurGrid.state = "won";
            if (data.gameMode === "battle_royale") {
                DemineurRoom.changeInfos("<p>Vous avez gagné !</p>");
            }
            DemineurGrid.changeSmiley("kk");
            DemineurRoom.updateRoomPlayers(data.players, data.gameMode);
            DemineurGrid.finishedGrid();
            break;
        case "grid_won_in_one":
            DemineurGrid.state = "won";
            DemineurGrid.changeSmiley("kk");
            DemineurRoom.changeInfos("<p>Victoire du serveur.</p>");
            DemineurGrid.finishedGrid();
            break;
        case "grid_won_in_one_battle_royale":
            DemineurGrid.state = "won";
            DemineurGrid.changeSmiley("kk");
            DemineurRoom.changeInfos("<p>Victoire du serveur. Votre prochaine grille comportera plus de mines.</p>");
            DemineurGrid.finishedGrid();
            break;
        case "attacked_battle_royale":
            DemineurRoom.changeInfos(`<p>${data.playerName} vous attaque ! Votre prochaine grille comportera plus de mines.</p>`);
            break;
        case "update_battle_royale_stats":
            DemineurRoom.updateRoomBattleRoyaleStats(data.playersRemaining, (data.knockOuts ? data.knockOuts : null), (data.badges ? data.badges : null));
            break;
        case "grid_lost_battle_royale":
            DemineurGrid.changeSmiley("ko");
            DemineurRoom.changeInfos(`<p>Grille perdue. Prochaine grille dans ${data.battleRoyaleTimeBeforeNextGrid / 1000} secondes.</p>`);
            break;
        case "grid_won_battle_royale":
            DemineurGrid.changeSmiley("kk");
            DemineurRoom.changeInfos(`<p>Grille gagnée. Prochaine grille dans ${data.battleRoyaleTimeBeforeNextGrid / 1000} secondes.</p>`);
            break;
        default:
            break;
    }
};

ws.onclose = () => {
    document.body.innerHTML = "<p>Erreur serveur.</p>";
};

ws.onerror = () => {
    document.body.innerHTML = "<p>Erreur serveur.</p>";
};

