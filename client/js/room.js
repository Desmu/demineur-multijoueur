class DemineurRoom {
    constructor () {
        this.id = "";
        this.players = [];
    }

    static sendRoom (ws) {
        ws.send(JSON.stringify({
            "method": "grid_create",
            "rowsTotal": Number(document.getElementById("rows_total").value),
            "colsTotal": Number(document.getElementById("cols_total").value),
            "bombsTotal": Number(document.getElementById("bombs_total").value),
            "botsTotal": Number(document.getElementById("bots_total").value),
            "timeBeforeStart": Number(document.getElementById("time_before_start").value),
            "startingCell": document.getElementById("starting_cell").checked,
            "gridStyle": document.getElementById("grid_style").value,
            "gameMode": document.getElementById("game_mode").value
        }));
    }

    static roomTested (ws) {
        const playerName = document.getElementById("player_name").value;
        let roomModeChecked = null;
        const roomModes = document.getElementsByName("room");
        roomModes.forEach((radio) => {
            if (radio.checked) {
                roomModeChecked = radio.value;
            }
        });
        if ((playerName !== "") && (roomModeChecked !== null)) {
            let roomCode = "";
            if (roomModeChecked === "join") {
                roomCode = document.getElementById("room_join_code").value;
            }
            ws.send(JSON.stringify({
                "method": "player_create",
                "name": playerName,
                "room": roomCode,
                "mode": roomModeChecked
            }));
        } else {
            alert("Nom non entré ou mode de jeu non choisi.");
        }
    }

    static roomCreated (ws, roomId, roomMode) {
        document.body.removeChild(document.getElementById("login"));
        document.getElementById("game").style.display = "block";
        alert(`Votre code est le ${roomId}`);
        if (roomMode === "public") {
            document.getElementById("title").innerHTML += ` - Salle ${roomId}`;
        }
        document.getElementById("grid_style").addEventListener("change", () => {
            ws.send(JSON.stringify({
                "method": "grid_style",
                "gridStyle": document.getElementById("grid_style").value
            }));
        });
        document.getElementById("submit").addEventListener("click", () => {
            if (!document.getElementById("submit").getAttribute("disabled")) {
                DemineurRoom.sendRoom(ws);
            }
        });
        document.getElementById("reset").addEventListener("click", () => {
            if (!document.getElementById("submit").getAttribute("disabled")) {
                DemineurRoom.sendRoom(ws);
            }
        });
    }

    static roomFound (roomId, roomMode) {
        document.body.removeChild(document.getElementById("login"));
        if (roomMode === "public") {
            document.getElementById("title").innerHTML += ` - Salle ${roomId}`;
        }
        document.body.removeChild(document.getElementById("game"));
    }

    static changeInfos (message) {
        document.getElementById("infos").innerHTML = message;
    }

    static formEventsAdmin () {
        const inputsNumber = ["rows_total", "cols_total", "bombs_total", "bots_total"];
        inputsNumber.forEach((inputNumber) => {
            document.getElementById(inputNumber).addEventListener("change", () => {
                if (document.getElementById(inputNumber).value > Number(document.getElementById(inputNumber).getAttribute("max"))) {
                    document.getElementById(inputNumber).value = document.getElementById(inputNumber).getAttribute("max");
                }
                DemineurRoom.changeBombAppearanceRate();
            });
        });
        document.getElementById("game_mode").addEventListener("change", DemineurRoom.battleRoyaleCheckPlayersAmount);
        document.getElementById("bots_total").addEventListener("change", DemineurRoom.battleRoyaleCheckPlayersAmount);
        DemineurRoom.battleRoyaleCheckPlayersAmount();
    }

    static changeBombAppearanceRate () {
        const rowsTotal = document.getElementById("rows_total").value;
        const colsTotal = document.getElementById("cols_total").value;
        const bombsTotal = document.getElementById("bombs_total").value;
        if (Number.isInteger(parseInt(rowsTotal, 10)) && Number.isInteger(parseInt(colsTotal, 10)) && Number.isInteger(parseInt(bombsTotal, 10))) {
            const area = rowsTotal * colsTotal;
            const rate = bombsTotal / area;
            if (rate > 0.5) {
                document.getElementById("starting_cell").checked = false;
                document.getElementById("starting_cell").disabled = true;
            } else {
                document.getElementById("starting_cell").checked = true;
                document.getElementById("starting_cell").disabled = false;
            }
        }
    }

    static intervalTimeBeforeStart (timeBeforeStart) {
        let secondes = timeBeforeStart / 1000;
        DemineurRoom.timeBeforeStart(secondes, timeBeforeStart);
        secondes -= 1;
        const interval = setInterval(() => {
            DemineurRoom.timeBeforeStart(secondes, timeBeforeStart);
            secondes -= 1;
            if (secondes === 0) {
                clearInterval(interval);
            }
        }, 1000);
    }

    static timeBeforeStart (secondes) {
        let pluriel = "s";
        if (secondes === 1) {
            pluriel = "";
        }
        if (secondes === 0) {
            DemineurRoom.changeInfos("");
        } else {
            DemineurRoom.changeInfos(`<p>${secondes} seconde${pluriel} avant le début de la manche.</p>`);
        }
    }

    static loadStyle () {
        document.getElementById("custom_stylesheet").addEventListener("load", () => {
            document.getElementById("stats").style.width = `${document.getElementById("demineur").offsetWidth}px`;
        });
    }

    static lockStyle () {
        if (document.getElementsByTagName("form").length !== 0) {
            document.getElementById("grid_style").disabled = true;
        }
    }

    static updateRoomPlayersAmount (playersAmount, gameMode) {
        if (gameMode === "battle_royale") {
            if (playersAmount > 1) {
                document.getElementById("players_amount").innerHTML = `<p>${playersAmount} joueurs et joueuses en ligne.</p>`;
                document.getElementById("submit").removeAttribute("disabled");
            } else {
                document.getElementById("players_amount").innerHTML = "";
                document.getElementById("submit").setAttribute("disabled", "");
            }
        } else if (playersAmount > 1) {
            document.getElementById("players_amount").innerHTML = `<p>${playersAmount} joueurs et joueuses en ligne.</p>`;
        } else {
            document.getElementById("players_amount").innerHTML = "";
        }
    }

    static updateRoomBattleRoyaleStats (playersRemaining, knockOuts, badges) {
        document.getElementById("br_ranking").innerHTML = playersRemaining;
        if ((knockOuts !== null) && (badges !== null)) {
            document.getElementById("br_kos").innerHTML = knockOuts;
            if (badges >= 30) {
                document.getElementById("br_badges_fragments").innerHTML = "4";
                document.getElementById("br_bonus").innerHTML = "100";
            } else if (badges >= 14) {
                document.getElementById("br_badges_fragments").innerHTML = "3";
                document.getElementById("br_bonus").innerHTML = "75";
            } else if (badges >= 6) {
                document.getElementById("br_badges_fragments").innerHTML = "2";
                document.getElementById("br_bonus").innerHTML = "50";
            } else if (badges >= 2) {
                document.getElementById("br_badges_fragments").innerHTML = "1";
                document.getElementById("br_bonus").innerHTML = "25";
            }
        }
    }

    static updateRoomPlayers (players, gameMode) {
        let table = "";
        switch (gameMode) {
            case "coop":
                table = `<tr>
                      <th>Joueur / joueuse</th>
                      <th>Cases révélées</th>
                      <th>Drapeaux bien placés</th>
                      <th>Mines explosées</th>
                    </tr>`;
                break;
            case "versus":
                table = `<tr>
                      <th>Joueur / joueuse</th>
                      <th>Défaites</th>
                      <th>Temps écoulé</th>
                      <th>Victoires</th>
                    </tr>`;
                break;
            case "battle_royale":
                table = `<tr>
                      <th>Classement</th>
                      <th>Joueur / joueuse</th>
                      <th>KOs</th>
                      <th>Grilles gagnées / perdues</th>
                    </tr>`;
                break;
            default:
                break;
        }
        players.forEach((player) => {
            let td = "<td>";
            if (player[4] === "won") {
                td = "<td class=\"success\">";
            } else if (player[4] === "lost") {
                td = "<td class=\"error\">";
            }
            const tr = `<tr>
                    ${td}${player[0]}</td>
                    ${td}${player[1]}</td>
                    ${td}${player[2]}</td>
                    ${td}${player[3]}</td>
                  </tr>`;
            table += tr;
        });
        document.getElementById("players").innerHTML = table;
    }

    static battleRoyaleFormEvents (ws, bool) {
        const radioButtons = ["br_killshots", "br_random", "br_badges", "br_counter"];
        radioButtons.forEach((radioButton) => {
            if (bool) {
                document.getElementById(radioButton).addEventListener("click", () => {
                    ws.send(JSON.stringify({
                        "method": "battle_royale_strategy",
                        "battleRoyaleStrategy": document.getElementById(radioButton).value
                    }));
                });
            } else {
                const oldElement = document.getElementById(radioButton);
                const newElement = oldElement.cloneNode(true);
                oldElement.parentNode.replaceChild(newElement, oldElement);
            }
        });
    }

    static battleRoyaleCheckPlayersAmount () {
        if (document.getElementById("game_mode").value === "battle_royale") {
            if ((document.getElementById("players_amount").innerHTML === "") && (parseInt(document.getElementById("bots_total").value, 10) === 0)) {
                document.getElementById("submit").setAttribute("disabled", "");
            } else {
                document.getElementById("submit").removeAttribute("disabled");
            }
        } else {
            document.getElementById("submit").removeAttribute("disabled");
        }
    }
}

export default DemineurRoom;

