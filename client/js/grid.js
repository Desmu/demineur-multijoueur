import DemineurRoom from "./room.js";

class DemineurGrid {
    constructor () {
        this.state = "";
        this.gridStyle = "";
        this.gameMode = "";
        this.colsTotal = 0;
        this.rowsTotal = 0;
        this.bombsTotal = 0;
        this.version = 0;
        this.brTime = 0;
        this.brTimeInterval = 0;
        this.brInterval = null;
    }

    static createGrid (ws, rowsTotal, colsTotal, bombsTotal, gridStyle, gameMode, battleRoyalePlayersAmount, battleRoyaleTimeAllowedBeforeLost) {
        DemineurRoom.changeInfos("<p>Bonne chance !</p>");
        DemineurGrid.state = "";
        DemineurGrid.rowsTotal = rowsTotal;
        DemineurGrid.colsTotal = colsTotal;
        DemineurGrid.bombsTotal = bombsTotal;
        DemineurGrid.version = 202009131645;
        if (DemineurGrid.gridStyle !== gridStyle) {
            DemineurGrid.changeStyle(gridStyle);
        }
        DemineurGrid.gameMode = gameMode;
        if ((gameMode === "battle_royale") && (battleRoyalePlayersAmount !== null)) {
            document.getElementById("br_ranking").innerHTML = battleRoyalePlayersAmount;
            document.getElementById("br_players_amount").innerHTML = battleRoyalePlayersAmount;
            document.getElementById("br_kos").innerHTML = 0;
            document.getElementById("br_badges_fragments").innerHTML = 0;
            document.getElementById("br_bonus").innerHTML = 0;
            DemineurRoom.battleRoyaleFormEvents(ws, true);
            document.getElementById("battle_royale_params").style.display = "block";
        } else if (gameMode !== "battle_royale") {
            DemineurRoom.battleRoyaleFormEvents(ws, false);
            document.getElementById("battle_royale_params").style.display = "none";
        }
        const demineur = document.getElementById("demineur");
        demineur.innerHTML = "";
        document.getElementById("mark").innerHTML = DemineurGrid.convertToDigits("mark", DemineurGrid.bombsTotal);
        DemineurGrid.changeSmiley("ok");
        for (let i = 0; i < DemineurGrid.rowsTotal; i += 1) {
            const tr = document.createElement("tr");
            tr.className = `tr-${i}`;
            for (let j = 0; j < DemineurGrid.colsTotal; j += 1) {
                const td = document.createElement("td");
                td.className = `td-${i}-${j}`;
                td.innerHTML = "<button> </button>";
                document.addEventListener("mouseup", () => {
                    if (document.getElementById("smiley")) {
                        DemineurGrid.changeSmiley("ok");
                    }
                });
                document.addEventListener("touchend", () => {
                    if (document.getElementById("smiley")) {
                        DemineurGrid.changeSmiley("ok");
                    }
                });
                td.firstChild.addEventListener("mousedown", (e) => {
                    if (e.buttons === 1) {
                        DemineurGrid.changeSmiley("oo");
                    }
                });
                td.firstChild.addEventListener("mouseup", (e) => {
                    DemineurGrid.clickGrid(e, ws, "");
                });
                let touchduration = "short";
                let touchtimeout = "";
                let istouched = false;
                td.firstChild.addEventListener("touchstart", (e) => {
                    e.preventDefault();
                    if (!istouched) {
                        istouched = true;
                        DemineurGrid.changeSmiley("oo");
                        touchtimeout = setTimeout(() => {
                            touchduration = "long";
                            DemineurGrid.clickGrid(e, ws, "mobileflag");
                            DemineurGrid.changeSmiley("ok");
                            window.navigator.vibrate(100);
                        }, 1000);
                    }
                }, {"passive": false});
                td.firstChild.addEventListener("touchend", (e) => {
                    if (istouched) {
                        istouched = false;
                        clearTimeout(touchtimeout);
                        if (touchduration === "short") {
                            DemineurGrid.clickGrid(e, ws, "mobilemine");
                        } else {
                            touchduration = "short";
                        }
                    }
                });
                tr.appendChild(td);
            }
            demineur.appendChild(tr);
        }
        demineur.addEventListener("contextmenu", (e) => {
            e.preventDefault();
        });
        clearInterval(DemineurGrid.brInterval);
        if (DemineurGrid.gameMode === "battle_royale") {
            DemineurGrid.brTimeInterval = battleRoyaleTimeAllowedBeforeLost;
            DemineurGrid.battleRoyaleResetInterval(battleRoyaleTimeAllowedBeforeLost);
        } else {
            document.getElementById("time").innerHTML = DemineurGrid.convertToDigits("time", "000");
        }
        document.getElementById("stats").style.width = `${demineur.offsetWidth}px`;
        document.getElementById("stats").style.opacity = "1";
        window.scrollTo(0, document.getElementById("stats").offsetTop);
    }

    static changeStyle (gridStyle) {
        document.getElementById("stats").style.opacity = "0";
        document.getElementById("demineur").innerHTML = "";
        DemineurGrid.gridStyle = gridStyle;
        document.getElementById("custom_stylesheet").href = `css/${gridStyle}/customstyle.css?v=${DemineurGrid.version}`;
        DemineurGrid.loadImages(gridStyle);
    }

    static loadImages (gridStyle) {
        const images = [
            "digits/0",
            "digits/1",
            "digits/2",
            "digits/3",
            "digits/4",
            "digits/5",
            "digits/6",
            "digits/7",
            "digits/8",
            "digits/9",
            "numbers/1",
            "numbers/2",
            "numbers/3",
            "numbers/4",
            "numbers/5",
            "numbers/6",
            "numbers/7",
            "numbers/8",
            "smileys/kk",
            "smileys/ko",
            "smileys/ok",
            "smileys/oo",
            "flag",
            "mine",
            "nomine",
            "question"
        ];
        let imagesloaded = "";
        images.forEach((image) => {
            imagesloaded += `<img src="css/${gridStyle}/${image}.png?v=${DemineurGrid.version}" alt="${image}">`;
        });
        document.getElementById("images").innerHTML = imagesloaded;
    }

    static convertToDigits (id, num) {
        const stringNum = num.toLocaleString(undefined, {"minimumIntegerDigits": 3, "useGrouping": false});
        document.getElementById(id).className = stringNum;
        let output = "";
        for (let i = 0; i < stringNum.length; i += 1) {
            output += `<img src="css/${DemineurGrid.gridStyle}/digits/${stringNum[i]}.png?v=${DemineurGrid.version}" alt="${stringNum[i]}">`;
        }
        return output;
    }

    static changeSmiley (smiley) {
        if ((smiley === "oo") || (smiley === "ok")) {
            if (DemineurGrid.state === "") {
                document.getElementById("smiley").src = `css/${DemineurGrid.gridStyle}/smileys/${smiley}.png?v=${DemineurGrid.version}`;
            }
        } else {
            document.getElementById("smiley").src = `css/${DemineurGrid.gridStyle}/smileys/${smiley}.png?v=${DemineurGrid.version}`;
        }
    }

    static clickGrid (e, ws, mobile) {
        DemineurGrid.changeSmiley("ok");
        if (e.target.offsetParent.className !== "") {
            const td = e.target.offsetParent;
            const [, row] = td.className.split("-");
            const [,, col] = td.className.split("-");
            if ((e.button === 0) || (mobile === "mobilemine")) {
                ws.send(JSON.stringify({
                    "method": "grid_click",
                    row,
                    col
                }));
            } else if ((e.button === 2) || (mobile === "mobileflag")) {
                ws.send(JSON.stringify({
                    "method": "grid_flag",
                    row,
                    col
                }));
                return false;
            }
        }
        return true;
    }

    static clickedGrid (cells) {
        let flagsAmount = DemineurGrid.bombsTotal;
        cells.forEach((cell) => {
            let newcell = "";
            const tagflag = `<img src="css/${DemineurGrid.gridStyle}/flag.png?v=${DemineurGrid.version}" alt="Flag">`;
            if (cell.hasFlag) {
                document.getElementsByClassName(`td-${cell.row}-${cell.col}`)[0].firstChild.innerHTML = tagflag;
                flagsAmount -= 1;
                document.getElementById("mark").innerHTML = DemineurGrid.convertToDigits("mark", flagsAmount);
            } else {
                if (cell.bombsAround === "exploded_mine") {
                    newcell = `<img src="css/${DemineurGrid.gridStyle}/mine.png?v=${DemineurGrid.version}" alt="Mine">`;
                    document.getElementsByClassName(`td-${cell.row}-${cell.col}`)[0].className += " error";
                } else if (cell.bombsAround === "mine") {
                    newcell = `<button><img src="css/${DemineurGrid.gridStyle}/mine.png?v=${DemineurGrid.version}" alt="Mine"></button>`;
                } else if (cell.bombsAround === "no_mine") {
                    newcell = `<button><img src="css/${DemineurGrid.gridStyle}/nomine.png?v=${DemineurGrid.version}" alt="No Mine"></button>`;
                } else if (cell.bombsAround !== 0) {
                    newcell = `<img src="css/${DemineurGrid.gridStyle}/numbers/${cell.bombsAround}.png?v=${DemineurGrid.version}" alt="${cell.bombsAround}">`;
                } else if (cell.bombsAround === 0) {
                    newcell = "";
                }
                document.getElementsByClassName(`td-${cell.row}-${cell.col}`)[0].innerHTML = newcell;
            }
        });
        if (DemineurGrid.gameMode === "battle_royale") {
            DemineurGrid.battleRoyaleResetInterval(DemineurGrid.brTimeInterval);
        }
    }

    static flaggedGrid (row, col, type, flags) {
        const tagflag = `<img src="css/${DemineurGrid.gridStyle}/flag.png?v=${DemineurGrid.version}" alt="Flag">`;
        if (type === "add") {
            document.getElementsByClassName(`td-${row}-${col}`)[0].firstChild.innerHTML = tagflag;
        } else {
            document.getElementsByClassName(`td-${row}-${col}`)[0].firstChild.innerHTML = "";
        }
        document.getElementById("mark").innerHTML = DemineurGrid.convertToDigits("mark", flags);
    }

    static finishedGrid () {
        Array.from(document.getElementsByTagName("button")).forEach((button) => {
            if (button.id !== "reset") {
                button.disabled = true;
                const newButton = button.cloneNode(true);
                button.parentNode.replaceChild(newButton, button);
            }
        });
        if (document.getElementsByTagName("form").length !== 0) {
            document.getElementById("grid_style").disabled = false;
        }
    }

    static battleRoyaleResetInterval (battleRoyaleTimeBeforeNextGrid) {
        clearInterval(DemineurGrid.brInterval);
        DemineurGrid.brTime = battleRoyaleTimeBeforeNextGrid / 1000 - 1;
        document.getElementById("time").innerHTML = DemineurGrid.convertToDigits("time", DemineurGrid.brTime);
        DemineurGrid.brTime -= 1;
        DemineurGrid.brInterval = setInterval(() => {
            document.getElementById("time").innerHTML = DemineurGrid.convertToDigits("time", DemineurGrid.brTime);
            DemineurGrid.brTime -= 1;
            if (DemineurGrid.brTime < 0) {
                clearInterval(DemineurGrid.brInterval);
            }
        }, 1000);
    }
}

export default DemineurGrid;

