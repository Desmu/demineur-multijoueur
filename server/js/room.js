import DemineurCell from "./cell.js";
import DemineurPlayer from "./player.js";

class DemineurRoom {
    constructor () {
        this.id = "";
        this.mode = "";
        this.grid = {};
        this.timeElapsed = 0;
        this.timeoutBeforeStart = null;
        this.timeInterval = null;
        this.players = [];
    }

    static getRoom (d, roomId) {
        let room = null;
        d.rooms.forEach((r) => {
            if (r.id === roomId) {
                room = r;
            }
        });
        return room;
    }

    static setRoom (d, room) {
        d.rooms.forEach((dr) => {
            if (dr.id === room.id) {
                dr = room;
            }
        });
        return false;
    }

    static sendRoomToPlayers (d, room, json) {
        const players = DemineurRoom.getRoomAllPlayers(d, room.id);
        if (players) {
            players.forEach((player) => {
                if ((!player.isBot) && (player.ws.readyState === 1)) {
                    player.ws.send(JSON.stringify(json));
                }
            });
        }
    }

    static createId (d) {
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (let i = 0; i < 4; i += 1) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        if (DemineurRoom.getRoom(d, text)) {
            text = DemineurRoom.createId(d);
        }
        return text;
    }

    static getRoomAllPlayers (d, roomId) {
        const room = DemineurRoom.getRoom(d, roomId);
        const players = [];
        if (room) {
            room.players.forEach((playerId) => {
                players.push(DemineurPlayer.getPlayer(d, playerId));
            });
        }
        return players;
    }

    static getRoomNotBotPlayers (d, roomId) {
        const room = DemineurRoom.getRoom(d, roomId);
        if (room) {
            const bots = [];
            room.players.forEach((playerId) => {
                const player = DemineurPlayer.getPlayer(d, playerId);
                if (!player.isBot) {
                    bots.push(DemineurPlayer.getPlayer(d, playerId));
                }
            });
            return bots;
        }
        return false;
    }

    static getRoomBotPlayers (d, roomId) {
        const room = DemineurRoom.getRoom(d, roomId);
        if (room) {
            const bots = [];
            room.players.forEach((playerId) => {
                const player = DemineurPlayer.getPlayer(d, playerId);
                if (player.isBot) {
                    bots.push(DemineurPlayer.getPlayer(d, playerId));
                }
            });
            return bots;
        }
        return false;
    }

    static getRoomGridPlayers (d, roomId) {
        const room = DemineurRoom.getRoom(d, roomId);
        if (room) {
            const players = [];
            room.players.forEach((playerId) => {
                const player = DemineurPlayer.getPlayer(d, playerId);
                if ((Object.entries(player.grid).length !== 0) && player.grid.constructor === Object) {
                    players.push(DemineurPlayer.getPlayer(d, playerId));
                }
            });
            return players;
        }
        return false;
    }

    static joinPublicRoom (d) {
        let smallestAmountPlayers = null;
        let smallestRoom = null;
        let brRoom = null;
        for (let i = d.rooms.length - 1; i > 0; i -= 1) {
            const j = Math.floor(Math.random() * (i + 1));
            [d.rooms[i], d.rooms[j]] = [d.rooms[j], d.rooms[i]];
        }
        d.rooms.forEach((room) => {
            if (room.mode === "public") {
                if (room.grid && (room.grid.gameMode === "battle_royale")) {
                    brRoom = room;
                } if (smallestAmountPlayers === null) {
                    smallestAmountPlayers = room.players.length;
                    smallestRoom = DemineurRoom.getRoom(d, room.id);
                } else if (smallestAmountPlayers > room.players.length) {
                    smallestAmountPlayers = room.players.length;
                    smallestRoom = DemineurRoom.getRoom(d, room.id);
                }
            }
        });
        if (brRoom !== null) {
            return brRoom;
        }
        if (smallestRoom !== null) {
            return smallestRoom;
        }
        return false;
    }

    static sendRoom (d, ws, player, room) {
        player.roomId = room.id;
        player.isAdmin = false;
        room.players.push(player.ws.uuid);
        d.players.push(player);
        ws.send(JSON.stringify({
            "method": "room_found",
            "roomId": room.id,
            "roomMode": room.mode
        }));
        DemineurRoom.sendRoomToPlayers(d, room, {
            "method": "players_update_amount",
            "playersAmount": DemineurRoom.getRoomNotBotPlayers(d, room.id).length,
            "gameMode": room.grid.gameMode
        });
        if ((room.grid.state === "") && (room.grid.gameMode !== "versus") && (room.grid.gameMode !== "battle_royale")) {
            const cells = [];
            for (let i = 0; i < room.grid.rowsTotal; i += 1) {
                for (let j = 0; j < room.grid.colsTotal; j += 1) {
                    if (room.grid.cells[i][j].clicked) {
                        cells.push(room.grid.cells[i][j]);
                    } else if (room.grid.cells[i][j].hasFlag) {
                        const restrictedCell = new DemineurCell();
                        restrictedCell.hasFlag = true;
                        restrictedCell.row = room.grid.cells[i][j].row;
                        restrictedCell.col = room.grid.cells[i][j].col;
                        cells.push(restrictedCell);
                    }
                }
            }
            ws.send(JSON.stringify({
                "method": "grid_current_state",
                cells,
                "rowsTotal": room.grid.rowsTotal,
                "colsTotal": room.grid.colsTotal,
                "gridStyle": room.grid.gridStyle,
                "bombsTotal": room.grid.bombsTotal,
                "gameMode": room.grid.gameMode
            }));
        } else if (room.grid.gameMode === "versus") {
            ws.send(JSON.stringify({
                "method": "wait_next_game_versus"
            }));
        } else if (room.grid.gameMode === "battle_royale") {
            ws.send(JSON.stringify({
                "method": "wait_next_battle_royale"
            }));
        } else {
            ws.send(JSON.stringify({
                "method": "wait_next_game"
            }));
        }
    }

    static battleRoyaleGetRemainingPlayers (d, roomId) {
        const room = DemineurRoom.getRoom(d, roomId);
        if (room) {
            const players = [];
            room.players.forEach((playerId) => {
                if (DemineurPlayer.getPlayer(d, playerId).brRanking === 0) {
                    players.push(DemineurPlayer.getPlayer(d, playerId));
                }
            });
            return players;
        }
        return false;
    }

    static battleRoyaleGetUnremainingPlayers (d, roomId) {
        const room = DemineurRoom.getRoom(d, roomId);
        if (room) {
            const players = [];
            room.players.forEach((playerId) => {
                if (DemineurPlayer.getPlayer(d, playerId).brRanking !== 0) {
                    players.push(DemineurPlayer.getPlayer(d, playerId));
                }
            });
            players.sort(DemineurRoom.battleRoyaleSortPlayersByRanking);
            return players;
        }
        return false;
    }

    static battleRoyaleSortPlayersByRanking (playerA, playerB) {
        if (playerA.brRanking < playerB.brRanking) {
            return -1;
        }
        if (playerA.brRanking > playerB.brRanking) {
            return 1;
        }
        return 0;
    }

    static battleRoyaleAttackRandomPlayer (d, roomId, attackingPlayer) {
        const players = DemineurRoom.battleRoyaleGetRemainingPlayers(d, roomId);
        let attackedPlayer = players[Math.floor(Math.random() * players.length)];
        while (attackingPlayer === attackedPlayer) {
            attackedPlayer = players[Math.floor(Math.random() * players.length)];
        }
        attackedPlayer.brBombAppearanceRate += (attackingPlayer.brBombAppearanceRate / 1.2) * (1 + DemineurPlayer.battleRoyaleBadgesToScore(attackingPlayer.brBadges));
        attackedPlayer.brLastTargetedByPlayersIds.push(attackingPlayer.ws.uuid);
        if (!attackedPlayer.isBot) {
            attackedPlayer.ws.send(JSON.stringify({
                "method": "attacked_battle_royale",
                "playerName": attackingPlayer.name
            }));
        }
    }

    static battleRoyaleAttackBadgesPlayer (d, roomId, attackingPlayer) {
        const badgedPlayers = DemineurRoom.battleRoyaleGetRoomBadgedPlayers(d, roomId);
        if (badgedPlayers && badgedPlayers[0] && badgedPlayers[1]) {
            let [attackedPlayer] = badgedPlayers;
            if (attackingPlayer === attackedPlayer) {
                [, attackedPlayer] = badgedPlayers;
            }
            attackedPlayer.brBombAppearanceRate += (attackingPlayer.brBombAppearanceRate / 1.2) * (1 + DemineurPlayer.battleRoyaleBadgesToScore(attackingPlayer.brBadges));
            attackedPlayer.brLastTargetedByPlayersIds.push(attackingPlayer.ws.uuid);
            if (!attackedPlayer.isBot) {
                attackedPlayer.ws.send(JSON.stringify({
                    "method": "attacked_battle_royale",
                    "playerName": attackingPlayer.name
                }));
            }
        } else {
            DemineurRoom.battleRoyaleAttackRandomPlayer(d, roomId, attackingPlayer);
        }
    }

    static battleRoyaleAttackCounterPlayer (d, roomId, attackingPlayer) {
        const attackedPlayersLength = attackingPlayer.brLastTargetedByPlayersIds.length;
        if (attackedPlayersLength === 0) {
            DemineurRoom.battleRoyaleAttackRandomPlayer(d, roomId, attackingPlayer);
        } else {
            attackingPlayer.brLastTargetedByPlayersIds.forEach((attackedPlayerId) => {
                const attackedPlayer = DemineurPlayer.getPlayer(d, attackedPlayerId);
                if (attackedPlayer.brRanking === 0) {
                    attackedPlayer.brBombAppearanceRate += (attackingPlayer.brBombAppearanceRate / 1.2) * (1 + DemineurPlayer.battleRoyaleBadgesToScore(attackingPlayer.brBadges)) + (attackedPlayersLength / 100);
                    attackedPlayer.brLastTargetedByPlayersIds.push(attackingPlayer.ws.uuid);
                    if (!attackedPlayer.isBot) {
                        attackedPlayer.ws.send(JSON.stringify({
                            "method": "attacked_battle_royale",
                            "playerName": attackingPlayer.name
                        }));
                    }
                }
            });
        }
    }

    static battleRoyaleAttackKillshotsPlayer (d, roomId, attackingPlayer) {
        const killshotedPlayers = DemineurRoom.battleRoyaleGetRoomKillshotedPlayers(d, roomId);
        let [attackedPlayer] = killshotedPlayers;
        if (attackingPlayer === attackedPlayer) {
            [, attackedPlayer] = killshotedPlayers;
        }
        attackedPlayer.brBombAppearanceRate += (attackingPlayer.brBombAppearanceRate / 1.2) * (1 + DemineurPlayer.battleRoyaleBadgesToScore(attackingPlayer.brBadges));
        attackedPlayer.brLastTargetedByPlayersIds.push(attackingPlayer.ws.uuid);
        if (!attackedPlayer.isBot) {
            attackedPlayer.ws.send(JSON.stringify({
                "method": "attacked_battle_royale",
                "playerName": attackingPlayer.name
            }));
        }
    }

    static battleRoyaleGetRoomBadgedPlayers (d, roomId) {
        let firstPlayer = null;
        let secondPlayer = null;
        let firstAmountOfBadges = 0;
        let secondAmountOfBadges = 0;
        const room = DemineurRoom.getRoom(d, roomId);
        if (room) {
            room.players.forEach((playerId) => {
                const player = DemineurPlayer.getPlayer(d, playerId);
                if (player.brRanking === 0) {
                    if (player.brBadges > firstAmountOfBadges) {
                        secondPlayer = firstPlayer;
                        if (firstPlayer === null) {
                            secondAmountOfBadges = 0;
                        } else {
                            secondAmountOfBadges = firstPlayer.brBadges;
                        }
                        firstPlayer = player;
                        firstAmountOfBadges = player.brBadges;
                    } else if (player.brBadges > secondAmountOfBadges) {
                        secondPlayer = player;
                        secondAmountOfBadges = player.brBadges;
                    }
                }
            });
            return [firstPlayer, secondPlayer];
        }
        return false;
    }

    static battleRoyaleGetRoomKillshotedPlayers (d, roomId) {
        let firstPlayer = null;
        let secondPlayer = null;
        let firstBombAppearanceRate = 0;
        let secondBombAppearanceRate = 0;
        const room = DemineurRoom.getRoom(d, roomId);
        if (room) {
            room.players.forEach((playerId) => {
                const player = DemineurPlayer.getPlayer(d, playerId);
                if (player.brRanking === 0) {
                    if (player.brBombAppearanceRate > firstBombAppearanceRate) {
                        secondPlayer = firstPlayer;
                        if (firstPlayer === null) {
                            secondBombAppearanceRate = 0;
                        } else {
                            secondBombAppearanceRate = firstPlayer.brBombAppearanceRate;
                        }
                        firstPlayer = player;
                        firstBombAppearanceRate = player.brBombAppearanceRate;
                    } else if (player.brBombAppearanceRate > secondBombAppearanceRate) {
                        secondPlayer = player;
                        secondBombAppearanceRate = player.brBombAppearanceRate;
                    }
                }
            });
            return [firstPlayer, secondPlayer];
        }
        return false;
    }
}

export default DemineurRoom;
