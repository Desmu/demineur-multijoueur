class DemineurCell {
    constructor () {
        this.row = 0;
        this.col = 0;
        this.bombsAround = 0;
        this.isBomb = false;
        this.hasFlag = false;
        this.clicked = false;
        this.flaggedByPlayerId = "";
    }

    static getFirstCellNotClickedNotFlagged (d, grid) {
        for (let i = 0; i < grid.rowsTotal; i += 1) {
            for (let j = 0; j < grid.colsTotal; j += 1) {
                if ((!grid.cells[i][j].clicked) && (!grid.cells[i][j].hasFlag)) {
                    return grid.cells[i][j];
                }
            }
        }
        return false;
    }

    static getBombsAroundCells (d, grid) {
        const cells = [];
        for (let i = 0; i < grid.rowsTotal; i += 1) {
            for (let j = 0; j < grid.colsTotal; j += 1) {
                if ((grid.cells[i][j].clicked) && (grid.cells[i][j].bombsAround !== 0)) {
                    cells.push(grid.cells[i][j]);
                }
            }
        }
        return cells;
    }

    static statsBombsAroundCell (d, grid, cell) {
        const cellsAround = [];
        const cellsNotClicked = [];
        const cellsFlagged = [];
        const stats = {
            "cellsWithBombsNotFlagged": [],
            "cellsWithoutBombs": [],
            "cellsCleared": false
        };
        if (((cell.row - 1) >= 0) && ((cell.col - 1) >= 0)) {
            cellsAround.push(grid.cells[cell.row - 1][cell.col - 1]);
        }
        if ((cell.row - 1) >= 0) {
            cellsAround.push(grid.cells[cell.row - 1][cell.col]);
        }
        if (((cell.row - 1) >= 0) && ((cell.col + 1) < grid.colsTotal)) {
            cellsAround.push(grid.cells[cell.row - 1][cell.col + 1]);
        }
        if ((cell.col - 1) >= 0) {
            cellsAround.push(grid.cells[cell.row][cell.col - 1]);
        }
        if ((cell.col + 1) < grid.colsTotal) {
            cellsAround.push(grid.cells[cell.row][cell.col + 1]);
        }
        if (((cell.row + 1) < grid.rowsTotal) && ((cell.col - 1) >= 0)) {
            cellsAround.push(grid.cells[cell.row + 1][cell.col - 1]);
        }
        if ((cell.row + 1) < grid.rowsTotal) {
            cellsAround.push(grid.cells[cell.row + 1][cell.col]);
        }
        if (((cell.row + 1) < grid.rowsTotal) && ((cell.col + 1) < grid.colsTotal)) {
            cellsAround.push(grid.cells[cell.row + 1][cell.col + 1]);
        }
        cellsAround.forEach((cellAround) => {
            if (!cellAround.clicked) {
                cellsNotClicked.push(cellAround);
                if (cellAround.hasFlag) {
                    cellsFlagged.push(cellAround);
                }
            }
        });
        if (cellsNotClicked.length === cell.bombsAround) {
            stats.cellsWithBombsNotFlagged = cellsNotClicked.filter((x) => !cellsFlagged.includes(x));
            if (stats.cellsWithBombsNotFlagged.length === 0) {
                stats.cellsCleared = true;
            }
        }
        if (cellsFlagged.length === cell.bombsAround) {
            stats.cellsWithoutBombs = cellsNotClicked.filter((x) => !cellsFlagged.includes(x));
            if (stats.cellsWithoutBombs.length === 0) {
                stats.cellsCleared = true;
            }
        }
        return stats;
    }

    static isCell (grid, row, col) {
        if (!Number.isInteger(parseInt(row, 10)) || !Number.isInteger(parseInt(col, 10))) {
            return false;
        } if ((Math.abs(row) < grid.rowsTotal) && (Math.abs(col) < grid.colsTotal)) {
            return true;
        }
        return false;
    }

    static getCellsBombsAround (d, grid) {
        const cellsBombsAround = [];
        for (let i = 0; i < grid.rowsTotal; i += 1) {
            for (let j = 0; j < grid.colsTotal; j += 1) {
                if ((grid.cells[i][j].clicked) && (grid.cells[i][j].bombsAround !== 0)) {
                    cellsBombsAround.push(grid.cells[i][j]);
                }
            }
        }
        return cellsBombsAround;
    }
}

export default DemineurCell;
