import DemineurRoom from "./room.js";

class DemineurPlayer {
    constructor () {
        this.ws = {};
        this.grid = {};
        this.name = "";
        this.roomId = "";
        this.cellsClicked = 0;
        this.flagsPlaced = 0;
        this.bombsExploded = 0;
        this.timeElapsed = 0;
        this.victories = 0;
        this.losses = 0;
        this.isAdmin = false;
        this.isBot = false;
        this.botTimeout = null;
        this.brTimeoutBeforeNextGrid = null;
        this.brPauseTimeout = null;
        this.brStrategy = "random";
        this.brLastTargetedByPlayersIds = [];
        this.brRanking = 0;
        this.brKos = 0;
        this.brBadges = 0;
        this.brWonGrids = 0;
        this.brLostGrids = 0;
        this.brBombAppearanceRate = 0;
        this.brBotWaitingTime = 200;
    }

    static getPlayer (d, playerId) {
        let player = null;
        d.players.forEach((p) => {
            if (p.ws.uuid === playerId) {
                player = p;
            }
        });
        return player;
    }

    static setPlayer (d, player) {
        d.players.forEach((p) => {
            if (p.ws.uuid === player.ws.uuid) {
                p = player;
            }
        });
        return false;
    }

    static createUUID () {
        function s4 () {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return `${s4() + s4()}-${s4()}`;
    }
    
    static flushPlayers(d) {
        d.players.forEach((p) => {
            if (p.ws.readyState === 3) {
                DemineurPlayer.deletePlayer(d, p.ws.uuid, true);
            }
        });
    }

    static createPlayer (d, ws, playerName, roomId, roomMode) {
        ws.uuid = DemineurPlayer.createUUID();
        const player = new DemineurPlayer();
        player.ws = ws;
        player.name = playerName;
        if (roomMode !== "join") {
            const room = new DemineurRoom();
            room.id = DemineurRoom.createId(d);
            room.mode = roomMode;
            player.roomId = room.id;
            player.isAdmin = true;
            room.players.push(player.ws.uuid);
            d.players.push(player);
            d.rooms.push(room);
            ws.send(JSON.stringify({
                "method": "room_created",
                "roomId": room.id,
                roomMode
            }));
        } else if (roomId) {
            const room = DemineurRoom.getRoom(d, roomId);
            if (room) {
                DemineurRoom.sendRoom(d, ws, player, room);
            } else {
                ws.send(JSON.stringify({
                    "method": "room_not_found"
                }));
            }
        } else {
            const room = DemineurRoom.joinPublicRoom(d);
            if (room) {
                DemineurRoom.sendRoom(d, ws, player, room);
            } else {
                ws.send(JSON.stringify({
                    "method": "no_public_room"
                }));
            }
        }
    }

    static deletePlayer (d, playerId, flushed = false) {
        const player = DemineurPlayer.getPlayer(d, playerId);
        if (player) {
            const room = DemineurRoom.getRoom(d, player.roomId);
            if (room) {
                clearTimeout(player.brTimeoutBeforeNextGrid);
                clearTimeout(player.brPauseTimeout);
                if (player.isAdmin) {
                    DemineurRoom.sendRoomToPlayers(d, room, {
                        "method": "room_destroyed"
                    });
                    DemineurPlayer.clearAllPlayersTimeouts(d, room);
                    const bots = DemineurRoom.getRoomBotPlayers(d, room.id);
                    if (bots) {
                        bots.forEach((roomBot) => {
                            d.players = d.players.filter((b) => b !== roomBot);
                        });
                    }
                    d.rooms = d.rooms.filter((r) => r !== room);
                    d.players = d.players.filter((p) => p !== player);
                } else {
                    DemineurRoom.getRoomAllPlayers(d, room.id).some((roomPlayer) => {
                        if (roomPlayer.ws.uuid === playerId) {
                            room.players = room.players.filter((pId) => pId !== roomPlayer.ws.uuid);
                            d.players = d.players.filter((p) => p !== player);
                            return true;
                        }
                        return false;
                    });
                    DemineurRoom.sendRoomToPlayers(d, room, {
                        "method": "players_update_amount",
                        "playersAmount": DemineurRoom.getRoomNotBotPlayers(d, room.id).length,
                        "gameMode": room.grid.gameMode
                    });
                    DemineurRoom.sendRoomToPlayers(d, room, {
                        "method": "update_battle_royale_stats",
                        "playersRemaining": DemineurRoom.getRoomGridPlayers(d, room.id).length
                    });
                }
            }
        }
        if (!flushed) {
            DemineurPlayer.flushPlayers(d);
        }
    }

    static createBots (d, room, botsTotal) {
        const bots = DemineurRoom.getRoomBotPlayers(d, room.id);
        if (bots) {
            const botsCurrentTotal = DemineurRoom.getRoomBotPlayers(d, room.id).length;
            if (botsTotal > botsCurrentTotal) {
                for (let i = 0; i < (botsTotal - botsCurrentTotal); i += 1) {
                    const player = new DemineurPlayer();
                    player.ws.uuid = DemineurPlayer.createUUID();
                    player.name = `BOT_${(`0${i}`).slice(-2)}`;
                    player.roomId = room.id;
                    player.isAdmin = false;
                    player.isBot = true;
                    room.players.push(player.ws.uuid);
                    d.players.push(player);
                }
            } else if (botsTotal < botsCurrentTotal) {
                for (let i = 0; i < (botsCurrentTotal - botsTotal); i += 1) {
                    room.players.pop();
                    d.players.pop();
                }
            }
        }
    }

    static clearAllPlayersTimeouts (d, room) {
        DemineurRoom.getRoomAllPlayers(d, room.id).forEach((player) => {
            clearTimeout(player.brTimeoutBeforeNextGrid);
            clearTimeout(player.brPauseTimeout);
            if (player.isBot) {
                clearTimeout(player.botTimeout);
            }
        });
    }

    static battleRoyaleStatsPlayersCreate (d, room) {
        if (room.grid.gameMode === "battle_royale") {
            const statsPlayers = [];
            DemineurRoom.getRoomAllPlayers(d, room.id).forEach((player) => {
                clearTimeout(player.brTimeoutBeforeNextGrid);
                clearTimeout(player.brPauseTimeout);
                player.brLastTargetedByPlayersIds = [];
                player.brRanking = 0;
                player.brKos = 0;
                player.brBadges = 0;
                player.brWonGrids = 0;
                player.brLostGrids = 0;
                player.brBombAppearanceRate = room.grid.bombAppearanceRate;
                statsPlayers.push([player.brRanking, player.name, player.brKos, player.brWonGrids, player.brLostGrids]);
            });
            return statsPlayers;
        }
        return false;
    }

    static battleRoyaleSetStrategy (d, playerId, battleRoyaleStrategy) {
        const player = DemineurPlayer.getPlayer(d, playerId);
        player.brStrategy = battleRoyaleStrategy;
        DemineurPlayer.setPlayer(d, player);
    }

    static battleRoyaleBadgesToScore (badges) {
        let score = 0;
        if (badges >= 30) {
            score = 1;
        } else if (badges >= 14) {
            score = 0.75;
        } else if (badges >= 6) {
            score = 0.5;
        } else if (badges >= 2) {
            score = 0.25;
        }
        return score;
    }
}

export default DemineurPlayer;
