import DemineurCell from "./cell.js";
import DemineurPlayer from "./player.js";
import DemineurRoom from "./room.js";

class DemineurGrid {
    constructor () {
        this.state = "";
        this.gridStyle = "";
        this.gameMode = "";
        this.cellStarting = false;
        this.rowsTotal = 0;
        this.colsTotal = 0;
        this.bombsTotal = 0;
        this.bombsRemaining = 0;
        this.bombAppearanceRate = 0;
        this.cellsTotal = 0;
        this.cellsRemaining = 0;
        this.flags = 0;
        this.cells = [];
        this.brTimeAllowedBeforeLost = 10000;
        this.brTimeBeforeNextGrid = 3000;
        this.brBombAppearanceRateMinimum = 0.05;
        this.brBombAppearanceRateMaximum = 0.33;
    }

    static botProcess (d, room, roomBot, checkedCells, noActionIteration) {
        let grid = null;
        switch (room.grid.gameMode) {
            case "coop":
                grid = room.grid;
                break;
            case "versus": case "battle_royale":
                grid = roomBot.grid;
                break;
            default:
                break;
        }
        if (grid.state === "") {
            if ((DemineurCell.getBombsAroundCells(d, grid).length !== 0) && ((room.grid.gameMode !== "battle_royale") || ((room.grid.gameMode === "battle_royale") && (noActionIteration >= (grid.brTimeAllowedBeforeLost / roomBot.brBotWaitingTime - roomBot.brBotWaitingTime * 2))))) {
                const cells = DemineurCell.getBombsAroundCells(d, grid).filter((x) => !checkedCells.includes(x));
                const currentCell = cells[Math.floor(Math.random() * cells.length)];
                const statsCell = DemineurCell.statsBombsAroundCell(d, grid, currentCell);
                if ((statsCell.cellsWithBombsNotFlagged.length === 0) && (statsCell.cellsWithoutBombs.length === 0)) {
                    noActionIteration += 1;
                    if (statsCell.cellsCleared) {
                        checkedCells.push(currentCell);
                    }
                    if (noActionIteration >= cells.length) {
                        const firstCell = DemineurCell.getFirstCellNotClickedNotFlagged(d, grid);
                        DemineurGrid.clickGrid(d, roomBot.ws, firstCell.row, firstCell.col);
                        noActionIteration = 0;
                    }
                } else {
                    noActionIteration = 0;
                    if (statsCell.cellsWithBombsNotFlagged.length !== 0) {
                        const cellWithBomb = statsCell.cellsWithBombsNotFlagged[Math.floor(Math.random() * statsCell.cellsWithBombsNotFlagged.length)];
                        DemineurGrid.flagGrid(d, roomBot.ws, cellWithBomb.row, cellWithBomb.col);
                        if (statsCell.cellsWithBombsNotFlagged.length === 1) {
                            checkedCells.push(currentCell);
                        }
                    } else if (statsCell.cellsWithoutBombs.length !== 0) {
                        const cellWithoutBomb = statsCell.cellsWithoutBombs[Math.floor(Math.random() * statsCell.cellsWithoutBombs.length)];
                        DemineurGrid.clickGrid(d, roomBot.ws, cellWithoutBomb.row, cellWithoutBomb.col);
                        if (statsCell.cellsWithoutBombs.length === 1) {
                            checkedCells.push(currentCell);
                        }
                    }
                }
            } else {
                const firstCell = DemineurCell.getFirstCellNotClickedNotFlagged(d, grid);
                DemineurGrid.clickGrid(d, roomBot.ws, firstCell.row, firstCell.col);
            }
            if (room.grid.gameMode === "battle_royale") {
                roomBot.brBotWaitingTime = Math.floor((Math.random() * 500) + 200);
            } else {
                roomBot.brBotWaitingTime = Math.floor((Math.random() * 2000) + 200);
            }
            roomBot.botTimeout = setTimeout(() => {
                DemineurGrid.botProcess(d, room, roomBot, checkedCells, noActionIteration);
            }, roomBot.brBotWaitingTime);
        }
    }

    static testBombCell (d, player, room, grid, testsAmount) {
        const row = Math.floor(Math.random() * grid.rowsTotal);
        const col = Math.floor(Math.random() * grid.colsTotal);
        if ((!grid.cells[row][col].isBomb) && ((grid.cells[row][col].bombsAround === 0) || (testsAmount > ((grid.rowsTotal * grid.colsTotal) / 4)))) {
            if (player === null) {
                const cells = DemineurGrid.neighboursGrid(d, {}, {}, room, [], row, col, true, false);
                DemineurRoom.sendRoomToPlayers(d, room, {
                    "method": "grid_clicked",
                    cells
                });
            } else {
                const cells = DemineurGrid.neighboursGrid(d, player.ws, player, room, [], row, col, true, true);
                if (!player.isBot) {
                    player.ws.send(JSON.stringify({
                        "method": "grid_clicked",
                        cells
                    }));
                }
            }
        } else if ((testsAmount + 1) < ((grid.rowsTotal * grid.colsTotal) / 2)) {
            DemineurGrid.testBombCell(d, player, room, grid, testsAmount + 1);
        }
    }

    static styleGrid (d, ws, style) {
        const player = DemineurPlayer.getPlayer(d, ws.uuid);
        if (player) {
            const room = DemineurRoom.getRoom(d, player.roomId);
            if (room && player.isAdmin) {
                DemineurRoom.sendRoomToPlayers(d, room, {
                    "method": "room_styled",
                    "gridStyle": style
                });
            }
        }
    }

    static checkGrid (d, ws, rowsTotal, colsTotal, bombsTotal, botsTotal, timeBeforeStart, gridStyle, gameMode, startingCell) {
        if (Number.isInteger(parseInt(rowsTotal, 10)) && Number.isInteger(parseInt(colsTotal, 10)) && Number.isInteger(parseInt(bombsTotal, 10)) && Number.isInteger(parseInt(botsTotal, 10)) && Number.isInteger(parseInt(timeBeforeStart, 10))) {
            if (rowsTotal > 50) {
                rowsTotal = 50;
            }
            if (colsTotal > 50) {
                colsTotal = 50;
            }
            if (bombsTotal > 2500) {
                bombsTotal = 2500;
            }
            if (botsTotal > 99) {
                botsTotal = 99;
            }
            if (bombsTotal < 1) {
                bombsTotal = 1;
            }
            if ((rowsTotal * colsTotal) < bombsTotal) {
                bombsTotal = rowsTotal * colsTotal;
            }
            if (gameMode === "battle_royale") {
                const testGrid = new DemineurGrid();
                const minBombs = parseInt(rowsTotal * colsTotal * testGrid.brBombAppearanceRateMinimum, 10);
                const maxBombs = parseInt(rowsTotal * colsTotal * testGrid.brBombAppearanceRateMaximum, 10);
                if (bombsTotal > maxBombs) {
                    bombsTotal = maxBombs;
                } else if (bombsTotal < minBombs) {
                    bombsTotal = minBombs;
                }
            }
            const player = DemineurPlayer.getPlayer(d, ws.uuid);
            if (player) {
                const room = DemineurRoom.getRoom(d, player.roomId);
                if (room && player.isAdmin && ((gameMode !== "battle_royale") || ((gameMode === "battle_royale") && ((botsTotal !== 0) || (room.players.length > 1))))) {
                    DemineurPlayer.clearAllPlayersTimeouts(d, room);
                    if ((Object.entries(room.grid).length === 0) && (room.grid.constructor === Object)) {
                        room.grid = new DemineurGrid();
                    }
                    room.grid = DemineurGrid.createGrid(room.grid, rowsTotal, colsTotal, bombsTotal, gridStyle, gameMode, startingCell);
                    room.timeElapsed = 0;
                    DemineurPlayer.createBots(d, room, botsTotal);
                    if (timeBeforeStart !== 0) {
                        timeBeforeStart *= 1000;
                        DemineurRoom.sendRoomToPlayers(d, room, {
                            "method": "grid_time_before_start",
                            timeBeforeStart
                        });
                    }
                    DemineurGrid.waitBeforeStart(d, room, timeBeforeStart);
                }
            }
        }
    }

    static createGrid (grid, rowsTotal, colsTotal, bombsTotal, gridStyle, gameMode, startingCell, brBombAppearanceRate = null) {
        grid.state = "";
        grid.cells = [];
        if (brBombAppearanceRate === null) {
            grid.rowsTotal = rowsTotal;
            grid.colsTotal = colsTotal;
            grid.gridStyle = gridStyle;
            grid.gameMode = gameMode;
            grid.cellStarting = startingCell;
            grid.bombsTotal = bombsTotal;
            grid.cellsTotal = grid.rowsTotal * grid.colsTotal;
            grid.bombAppearanceRate = grid.bombsTotal / grid.cellsTotal;
        } else {
            grid.bombAppearanceRate = brBombAppearanceRate;
            grid.bombsTotal = parseInt(grid.bombAppearanceRate * grid.cellsTotal, 10);
        }
        grid.bombsRemaining = grid.bombsTotal;
        grid.flags = grid.bombsTotal;
        grid.cellsRemaining = grid.cellsTotal;
        for (let i = 0; i < grid.rowsTotal; i += 1) {
            const line = [];
            for (let j = 0; j < grid.colsTotal; j += 1) {
                const cell = new DemineurCell();
                cell.clicked = false;
                cell.row = i;
                cell.col = j;
                if ((Math.random() <= grid.bombAppearanceRate) && (grid.bombsRemaining !== 0)) {
                    cell.isBomb = true;
                    grid.bombsRemaining -= 1;
                } else {
                    cell.isBomb = false;
                }
                line.push(cell);
                grid.cellsRemaining -= 1;
            }
            grid.cells.push(line);
        }
        while (grid.bombsRemaining !== 0) {
            const i = Math.floor(Math.random() * grid.rowsTotal);
            const j = Math.floor(Math.random() * grid.colsTotal);
            if (!grid.cells[i][j].isBomb) {
                grid.cells[i][j].isBomb = true;
                grid.bombsRemaining -= 1;
            }
        }
        grid.cellsRemaining = grid.cellsTotal;
        for (let i = 0; i < grid.rowsTotal; i += 1) {
            for (let j = 0; j < grid.colsTotal; j += 1) {
                if (grid.cells[i][j].isBomb) {
                    if (((i - 1) >= 0) && ((j - 1) >= 0)) {
                        grid.cells[i - 1][j - 1].bombsAround += 1;
                    }
                    if ((i - 1) >= 0) {
                        grid.cells[i - 1][j].bombsAround += 1;
                    }
                    if (((i - 1) >= 0) && ((j + 1) < grid.colsTotal)) {
                        grid.cells[i - 1][j + 1].bombsAround += 1;
                    }
                    if ((j - 1) >= 0) {
                        grid.cells[i][j - 1].bombsAround += 1;
                    }
                    if ((j + 1) < grid.colsTotal) {
                        grid.cells[i][j + 1].bombsAround += 1;
                    }
                    if (((i + 1) < grid.rowsTotal) && ((j - 1) >= 0)) {
                        grid.cells[i + 1][j - 1].bombsAround += 1;
                    }
                    if ((i + 1) < grid.rowsTotal) {
                        grid.cells[i + 1][j].bombsAround += 1;
                    }
                    if (((i + 1) < grid.rowsTotal) && ((j + 1) < grid.colsTotal)) {
                        grid.cells[i + 1][j + 1].bombsAround += 1;
                    }
                }
            }
        }
        return grid;
    }

    static waitBeforeStart (d, room, timeBeforeStart) {
        room.timeoutBeforeStart = setTimeout(() => {
            clearInterval(room.timeInterval);
            if (room.players.length !== 0) {
                let battleRoyalePlayersAmount = null;
                if (room.grid.gameMode === "battle_royale") {
                    battleRoyalePlayersAmount = DemineurPlayer.battleRoyaleStatsPlayersCreate(d, room).length;
                } else {
                    room.timeInterval = setInterval(() => {
                        room.timeElapsed += 1;
                        DemineurRoom.sendRoomToPlayers(d, room, {
                            "method": "grid_time_elapsed",
                            "timeElapsed": room.timeElapsed
                        });
                    }, 1000);
                }
                DemineurRoom.setRoom(d, room);
                DemineurRoom.sendRoomToPlayers(d, room, {
                    "method": "grid_ready",
                    "colsTotal": room.grid.colsTotal,
                    "rowsTotal": room.grid.rowsTotal,
                    "bombsTotal": room.grid.bombsTotal,
                    "gridStyle": room.grid.gridStyle,
                    "gameMode": room.grid.gameMode,
                    battleRoyalePlayersAmount,
                    "battleRoyaleTimeAllowedBeforeLost": room.grid.brTimeAllowedBeforeLost
                });
                if (room.grid.cellStarting) {
                    DemineurGrid.testBombCell(d, null, room, room.grid, 0);
                }
                if ((room.grid.gameMode === "versus") || (room.grid.gameMode === "battle_royale")) {
                    room.players.forEach((playerId) => {
                        const player = DemineurPlayer.getPlayer(d, playerId);
                        player.grid = JSON.parse(JSON.stringify(room.grid));
                        player.timeElapsed = 0;
                        if (room.grid.gameMode === "battle_royale") {
                            clearTimeout(player.brTimeoutBeforeNextGrid);
                            player.brTimeoutBeforeNextGrid = setTimeout(() => {
                                DemineurGrid.lostGrid(d, player.ws, player, room, null, null);
                            }, player.grid.brTimeAllowedBeforeLost);
                        }
                        DemineurPlayer.setPlayer(d, player);
                    });
                }
                const bots = DemineurRoom.getRoomBotPlayers(d, room.id);
                if (bots) {
                    bots.forEach((roomBot) => {
                        DemineurGrid.botProcess(d, room, roomBot, [], 0);
                    });
                }
            }
        }, timeBeforeStart);
    }

    static neighboursGrid (d, ws, player, room, cells, row, col, isTestingCell, isBattleRoyaleStep) {
        let grid = null;
        switch (room.grid.gameMode) {
            case "coop":
                grid = room.grid;
                grid.cells[row][col].clicked = true;
                cells.push({
                    row,
                    col,
                    "bombsAround": grid.cells[row][col].bombsAround,
                    "hasFlag": grid.cells[row][col].hasFlag
                });
                break;
            case "versus": case "battle_royale":
                if ((isTestingCell) && (!isBattleRoyaleStep)) {
                    grid = room.grid;
                    grid.cells[row][col].clicked = true;
                    cells.push({
                        row,
                        col,
                        "bombsAround": grid.cells[row][col].bombsAround,
                        "hasFlag": grid.cells[row][col].hasFlag
                    });
                } else {
                    grid = player.grid;
                    grid.cells[row][col].clicked = true;
                    cells.push({
                        row,
                        col,
                        "bombsAround": grid.cells[row][col].bombsAround,
                        "hasFlag": grid.cells[row][col].hasFlag
                    });
                }
                break;
            default:
                break;
        }
        grid.cellsRemaining -= 1;
        if (grid.cellsRemaining === grid.bombsTotal) {
            DemineurGrid.wonGrid(d, ws, player, room, row, col, isTestingCell);
        }
        if (grid.cells[row][col].bombsAround === 0) {
            row = parseInt(row, 10);
            col = parseInt(col, 10);
            if (((row - 1) >= 0) && ((col - 1) >= 0)) {
                if ((!grid.cells[row - 1][col - 1].clicked) && (!grid.cells[row - 1][col - 1].hasFlag)) {
                    DemineurGrid.neighboursGrid(d, ws, player, room, cells, row - 1, col - 1, isTestingCell, isBattleRoyaleStep);
                }
            }
            if ((row - 1) >= 0) {
                if ((!grid.cells[row - 1][col].clicked) && (!grid.cells[row - 1][col].hasFlag)) {
                    DemineurGrid.neighboursGrid(d, ws, player, room, cells, row - 1, col, isTestingCell, isBattleRoyaleStep);
                }
            }
            if (((row - 1) >= 0) && ((col + 1) < grid.colsTotal)) {
                if ((!grid.cells[row - 1][col + 1].clicked) && (!grid.cells[row - 1][col + 1].hasFlag)) {
                    DemineurGrid.neighboursGrid(d, ws, player, room, cells, row - 1, col + 1, isTestingCell, isBattleRoyaleStep);
                }
            }
            if ((col - 1) >= 0) {
                if ((!grid.cells[row][col - 1].clicked) && (!grid.cells[row][col - 1].hasFlag)) {
                    DemineurGrid.neighboursGrid(d, ws, player, room, cells, row, col - 1, isTestingCell, isBattleRoyaleStep);
                }
            }
            if ((col + 1) < grid.colsTotal) {
                if ((!grid.cells[row][col + 1].clicked) && (!grid.cells[row][col + 1].hasFlag)) {
                    DemineurGrid.neighboursGrid(d, ws, player, room, cells, row, col + 1, isTestingCell, isBattleRoyaleStep);
                }
            }
            if (((row + 1) < grid.rowsTotal) && ((col - 1) >= 0)) {
                if ((!grid.cells[row + 1][col - 1].clicked) && (!grid.cells[row + 1][col - 1].hasFlag)) {
                    DemineurGrid.neighboursGrid(d, ws, player, room, cells, row + 1, col - 1, isTestingCell, isBattleRoyaleStep);
                }
            }
            if ((row + 1) < grid.rowsTotal) {
                if ((!grid.cells[row + 1][col].clicked) && (!grid.cells[row + 1][col].hasFlag)) {
                    DemineurGrid.neighboursGrid(d, ws, player, room, cells, row + 1, col, isTestingCell, isBattleRoyaleStep);
                }
            }
            if (((row + 1) < grid.rowsTotal) && ((col + 1) < grid.colsTotal)) {
                if ((!grid.cells[row + 1][col + 1].clicked) && (!grid.cells[row + 1][col + 1].hasFlag)) {
                    DemineurGrid.neighboursGrid(d, ws, player, room, cells, row + 1, col + 1, isTestingCell, isBattleRoyaleStep);
                }
            }
        }
        return cells;
    }

    static clickGrid (d, ws, row, col) {
        const player = DemineurPlayer.getPlayer(d, ws.uuid);
        if (player) {
            const room = DemineurRoom.getRoom(d, player.roomId);
            if (room && ((room.grid.gameMode !== "battle_royale") || ((room.grid.gameMode === "battle_royale") && (player.grid.state === "")))) {
                const cell = DemineurCell.isCell(room.grid, row, col);
                if (cell && (room.grid.state === "")) {
                    if (room.grid.gameMode === "battle_royale") {
                        clearTimeout(player.brTimeoutBeforeNextGrid);
                        player.brTimeoutBeforeNextGrid = setTimeout(() => {
                            DemineurGrid.lostGrid(d, player.ws, player, room, null, null);
                        }, player.grid.brTimeAllowedBeforeLost);
                    }
                    let grid = null;
                    switch (room.grid.gameMode) {
                        case "coop":
                            grid = room.grid;
                            break;
                        case "versus": case "battle_royale":
                            grid = player.grid;
                            break;
                        default:
                            break;
                    }
                    if (grid.cells[row][col].isBomb) {
                        DemineurGrid.lostGrid(d, ws, player, room, row, col);
                    } else {
                        if (room.grid.gameMode === "coop") {
                            player.cellsClicked += 1;
                        }
                        const cells = DemineurGrid.neighboursGrid(d, ws, player, room, [], row, col, false, false);
                        switch (room.grid.gameMode) {
                            case "coop":
                                DemineurRoom.sendRoomToPlayers(d, room, {
                                    "method": "grid_clicked",
                                    cells
                                });
                                break;
                            case "versus": case "battle_royale":
                                if (!player.isBot) {
                                    ws.send(JSON.stringify({
                                        "method": "grid_clicked",
                                        cells
                                    }));
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    static flagGrid (d, ws, row, col) {
        const player = DemineurPlayer.getPlayer(d, ws.uuid);
        if (player) {
            const room = DemineurRoom.getRoom(d, player.roomId);
            if (room) {
                if (DemineurCell.isCell(room.grid, row, col) && (room.grid.state === "")) {
                    DemineurGrid.flaggedGrid(d, ws, player, room, row, col);
                }
            }
        }
    }

    static flaggedGrid (d, ws, player, room, row, col) {
        let grid = null;
        switch (room.grid.gameMode) {
            case "coop":
                grid = room.grid;
                break;
            case "versus": case "battle_royale":
                grid = player.grid;
                break;
            default:
                break;
        }
        if ((!grid.cells[row][col].hasFlag) && (grid.flags > 0)) {
            if (room.grid.gameMode === "coop") {
                player.flagsPlaced += 1;
            }
            grid.cells[row][col].hasFlag = true;
            grid.flags -= 1;
            switch (room.grid.gameMode) {
                case "coop":
                    grid.cells[row][col].flaggedByPlayerId = player.ws.uuid;
                    DemineurRoom.sendRoomToPlayers(d, room, {
                        "method": "grid_flagged",
                        row,
                        col,
                        "type": "add",
                        "flags": grid.flags
                    });
                    break;
                case "versus": case "battle_royale":
                    if (!player.isBot) {
                        ws.send(JSON.stringify({
                            "method": "grid_flagged",
                            row,
                            col,
                            "type": "add",
                            "flags": grid.flags
                        }));
                    }
                    break;
                default:
                    break;
            }
        } else if (grid.cells[row][col].hasFlag) {
            if (room.grid.gameMode === "coop") {
                player.flagsPlaced -= 1;
            }
            grid.cells[row][col].hasFlag = false;
            grid.flags += 1;
            switch (room.grid.gameMode) {
                case "coop":
                    grid.cells[row][col].flaggedByPlayerId = "";
                    DemineurRoom.sendRoomToPlayers(d, room, {
                        "method": "grid_flagged",
                        row,
                        col,
                        "type": "remove",
                        "flags": grid.flags
                    });
                    break;
                case "versus": case "battle_royale":
                    if (!player.isBot) {
                        ws.send(JSON.stringify({
                            "method": "grid_flagged",
                            row,
                            col,
                            "type": "remove",
                            "flags": grid.flags
                        }));
                    }
                    break;
                default:
                    break;
            }
        }
    }

    static showBombs (d, ws, player, room, row, col) {
        let grid = null;
        switch (room.grid.gameMode) {
            case "coop":
                grid = room.grid;
                break;
            case "versus": case "battle_royale":
                grid = player.grid;
                break;
            default:
                break;
        }
        const cells = [];
        for (let i = 0; i < grid.rowsTotal; i += 1) {
            for (let j = 0; j < grid.colsTotal; j += 1) {
                if (grid.cells[i][j].isBomb) {
                    if (row && col && (i === Number(row)) && (j === Number(col))) {
                        cells.push({
                            "row": i,
                            "col": j,
                            "bombsAround": "exploded_mine"
                        });
                    } else {
                        cells.push({
                            "row": i,
                            "col": j,
                            "bombsAround": "mine"
                        });
                    }
                } else if (grid.cells[i][j].hasFlag) {
                    if (room.grid.gameMode === "coop") {
                        const p = DemineurPlayer.getPlayer(d, grid.cells[i][j].flaggedByPlayerId);
                        p.flagsPlaced -= 1;
                    }
                    cells.push({
                        "row": i,
                        "col": j,
                        "bombsAround": "no_mine"
                    });
                }
            }
        }
        switch (room.grid.gameMode) {
            case "coop":
                DemineurRoom.sendRoomToPlayers(d, room, {
                    "method": "grid_show_bombs",
                    cells
                });
                break;
            case "versus": case "battle_royale":
                if (!player.isBot) {
                    ws.send(JSON.stringify({
                        "method": "grid_show_bombs",
                        cells
                    }));
                }
                break;
            default:
                break;
        }
    }

    static battleRoyaleSendNewGrid (d, player, room) {
        clearTimeout(player.brPauseTimeout);
        player.brPauseTimeout = setTimeout(() => {
            clearTimeout(player.brTimeoutBeforeNextGrid);
            if ((player.isBot) || (player.ws.readyState !== 3)) {
                player.brTimeoutBeforeNextGrid = setTimeout(() => {
                    DemineurGrid.lostGrid(d, player.ws, player, room, null, null);
                }, player.grid.brTimeAllowedBeforeLost);
                player.grid = DemineurGrid.createGrid(player.grid, null, null, null, null, null, null, player.brBombAppearanceRate);
                if (!player.isBot) {
                    player.ws.send(JSON.stringify({
                        "method": "grid_ready",
                        "colsTotal": player.grid.colsTotal,
                        "rowsTotal": player.grid.rowsTotal,
                        "bombsTotal": player.grid.bombsTotal,
                        "gridStyle": player.grid.gridStyle,
                        "gameMode": player.grid.gameMode,
                        "battleRoyaleTimeAllowedBeforeLost": player.grid.brTimeAllowedBeforeLost
                    }));
                }
                if (player.grid.cellStarting) {
                    DemineurGrid.testBombCell(d, player, room, player.grid, 0);
                }
            }
        }, player.grid.brTimeBeforeNextGrid);
    }

    static lostGrid (d, ws, player, room, row, col) {
        DemineurGrid.showBombs(d, ws, player, room, row, col);
        const players = [];
        switch (room.grid.gameMode) {
            case "coop":
                clearInterval(room.timeInterval);
                player.cellsClicked += 1;
                player.bombsExploded += 1;
                room.grid.state = "lost";
                room.players.forEach((playerId) => {
                    const p = DemineurPlayer.getPlayer(d, playerId);
                    if (player.name === p.name) {
                        players.push([
                            p.name,
                            p.cellsClicked,
                            p.flagsPlaced,
                            p.bombsExploded,
                            "lost"
                        ]);
                    } else {
                        players.push([
                            p.name,
                            p.cellsClicked,
                            p.flagsPlaced,
                            p.bombsExploded,
                            ""
                        ]);
                    }
                });
                DemineurRoom.sendRoomToPlayers(d, room, {
                    "method": "grid_lost",
                    players,
                    "gameMode": room.grid.gameMode
                });
                break;
            case "versus":
                player.losses += 1;
                player.timeElapsed = room.timeElapsed;
                player.grid.state = "lost";
                room.players.forEach((playerId) => {
                    const p = DemineurPlayer.getPlayer(d, playerId);
                    players.push([
                        p.name,
                        p.losses,
                        p.timeElapsed,
                        p.victories,
                        p.grid.state
                    ]);
                });
                DemineurRoom.sendRoomToPlayers(d, room, {
                    "method": "players_update",
                    players,
                    "gameMode": room.grid.gameMode
                });
                if (!player.isBot) {
                    ws.send(JSON.stringify({
                        "method": "grid_lost",
                        players,
                        "gameMode": room.grid.gameMode
                    }));
                }
                break;
            case "battle_royale":
                player.brLostGrids += 1;
                player.grid.state = "lost";
                player.brBombAppearanceRate *= 1.2;
                if (player.brBombAppearanceRate > player.grid.brBombAppearanceRateMaximum) {
                    clearTimeout(player.brTimeoutBeforeNextGrid);
                    player.brRanking = DemineurRoom.battleRoyaleGetRemainingPlayers(d, room.id).length;
                    if (player.brLastTargetedByPlayersIds.length !== 0) {
                        let lastAttackingPlayer = null;
                        let index = player.brLastTargetedByPlayersIds.length;
                        while (index !== 0) {
                            if (DemineurPlayer.getPlayer(d, player.brLastTargetedByPlayersIds[index])) {
                                lastAttackingPlayer = DemineurPlayer.getPlayer(d, player.brLastTargetedByPlayersIds[index]);
                                break;
                            } else {
                                index -= 1;
                            }
                        }
                        if (lastAttackingPlayer !== null) {
                            lastAttackingPlayer.brKos += 1;
                            lastAttackingPlayer.brBadges += player.brBadges + 1;
                            if (!lastAttackingPlayer.isBot) {
                                lastAttackingPlayer.ws.send(JSON.stringify({
                                    "method": "update_battle_royale_stats",
                                    "playersRemaining": DemineurRoom.getRoomGridPlayers(d, room.id).length,
                                    "knockOuts": lastAttackingPlayer.brKos,
                                    "badges": lastAttackingPlayer.brBadges
                                }));
                            }
                        }
                    }
                    player.grid = {};
                    const remainingPlayers = DemineurRoom.battleRoyaleGetRemainingPlayers(d, room.id);
                    if (remainingPlayers.length === 1) {
                        clearInterval(room.timeInterval);
                        clearTimeout(remainingPlayers[0].brTimeoutBeforeNextGrid);
                        clearTimeout(remainingPlayers[0].brPauseTimeout);
                        remainingPlayers[0].brRanking = 1;
                        room.grid.state = "won";
                        if (remainingPlayers[0].isBot) {
                            clearTimeout(remainingPlayers[0].botTimeout);
                        } else {
                            remainingPlayers[0].ws.send(JSON.stringify({
                                "method": "grid_won",
                                players,
                                "gameMode": room.grid.gameMode
                            }));
                        }
                    }
                    const unremainingPlayers = DemineurRoom.battleRoyaleGetUnremainingPlayers(d, room.id);
                    if (unremainingPlayers) {
                        unremainingPlayers.forEach((p) => {
                            players.push([
                                p.brRanking,
                                p.name,
                                p.brKos,
                                (`${p.brWonGrids} / ${p.brLostGrids}`),
                                ""
                            ]);
                        });
                    }
                    if (!player.isBot) {
                        ws.send(JSON.stringify({
                            "method": "grid_lost",
                            players,
                            "gameMode": room.grid.gameMode
                        }));
                    }
                    DemineurRoom.sendRoomToPlayers(d, room, {
                        "method": "players_update",
                        players,
                        "gameMode": room.grid.gameMode
                    });
                    DemineurRoom.sendRoomToPlayers(d, room, {
                        "method": "update_battle_royale_stats",
                        "playersRemaining": DemineurRoom.getRoomGridPlayers(d, room.id).length
                    });
                } else {
                    if (!player.isBot) {
                        ws.send(JSON.stringify({
                            "method": "grid_lost_battle_royale",
                            "battleRoyaleTimeBeforeNextGrid": player.grid.brTimeBeforeNextGrid
                        }));
                    }
                    DemineurGrid.battleRoyaleSendNewGrid(d, player, room);
                }
                break;
            default:
                break;
        }
    }

    static wonGrid (d, ws, player, room, row, col, isTestingCell) {
        if (!isTestingCell) {
            const players = [];
            switch (room.grid.gameMode) {
                case "coop":
                    clearInterval(room.timeInterval);
                    room.grid.state = "won";
                    room.players.forEach((playerId) => {
                        const p = DemineurPlayer.getPlayer(d, playerId);
                        players.push([
                            p.name,
                            p.cellsClicked,
                            p.flagsPlaced,
                            p.bombsExploded,
                            "won"
                        ]);
                    });
                    DemineurRoom.sendRoomToPlayers(d, room, {
                        "method": "grid_won",
                        players,
                        "gameMode": room.grid.gameMode
                    });
                    break;
                case "versus":
                    player.victories += 1;
                    player.timeElapsed = room.timeElapsed;
                    player.grid.state = "won";
                    room.players.forEach((playerId) => {
                        const p = DemineurPlayer.getPlayer(d, playerId);
                        players.push([
                            p.name,
                            p.losses,
                            p.timeElapsed,
                            p.victories,
                            p.grid.state
                        ]);
                    });
                    DemineurRoom.sendRoomToPlayers(d, room, {
                        "method": "players_update",
                        players,
                        "gameMode": room.grid.gameMode
                    });
                    if (!player.isBot) {
                        ws.send(JSON.stringify({
                            "method": "grid_won",
                            players,
                            "gameMode": room.grid.gameMode
                        }));
                    }
                    break;
                case "battle_royale":
                    player.brWonGrids += 1;
                    player.grid.state = "won";
                    player.brBombAppearanceRate *= 0.8;
                    if (player.brBombAppearanceRate < player.grid.brBombAppearanceRateMinimum) {
                        player.brBombAppearanceRate = player.grid.brBombAppearanceRateMinimum;
                    }
                    switch (player.brStrategy) {
                    case "random":
                        DemineurRoom.battleRoyaleAttackRandomPlayer(d, room.id, player);
                        break;
                    case "badges":
                        DemineurRoom.battleRoyaleAttackBadgesPlayer(d, room.id, player);
                        break;
                    case "counter":
                        DemineurRoom.battleRoyaleAttackCounterPlayer(d, room.id, player);
                        break;
                    case "killshots":
                        DemineurRoom.battleRoyaleAttackKillshotsPlayer(d, room.id, player);
                        break;
                    default:
                        break;
                    }
                    if (!player.isBot) {
                        ws.send(JSON.stringify({
                            "method": "grid_won_battle_royale",
                            "battleRoyaleTimeBeforeNextGrid": player.grid.brTimeBeforeNextGrid
                        }));
                    }
                    DemineurGrid.battleRoyaleSendNewGrid(d, player, room);
                    break;
                default:
                    break;
            }
        } else if (room.grid.gameMode === "battle_royale") {
            player.grid.state = "won";
            player.brBombAppearanceRate += 0.01;
            if (!player.isBot) {
                ws.send(JSON.stringify({
                    "method": "grid_won_in_one_battle_royale"
                }));
            }
            DemineurGrid.battleRoyaleSendNewGrid(d, player, room);
        } else {
            clearInterval(room.timeInterval);
            DemineurRoom.sendRoomToPlayers(d, room, {
                "method": "grid_won_in_one"
            });
        }
    }
}

export default DemineurGrid;
