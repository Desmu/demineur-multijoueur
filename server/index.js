import fs from "fs";
import https from "https";
import path from "path";
import url from "url";
import WebSocket from "ws";
import { WebSocketServer } from 'ws';

import Demineur from "./js/demineur.js";
import DemineurGrid from "./js/grid.js";
import DemineurPlayer from "./js/player.js";

const configuration = JSON.parse(fs.readFileSync(path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), "./configuration.json"), "utf8"));
const d = new Demineur();
let wss = null;

if (configuration.env === "dev") {
    wss = new WebSocketServer({"port": configuration.serverPort});
} else if (configuration.env === "prod") {
    const server = new https.createServer({
        "cert": fs.readFileSync(`${configuration.certPem}`),
        "key": fs.readFileSync(`${configuration.keyPem}`)
    });
    wss = new WebSocketServer({server});
    server.listen(configuration.serverPort);
}

function parseMessage (webmessage) {
    // Parses the websocket received from client.
    const data = JSON.parse(webmessage);
    Object.keys(data).forEach((key) => {
        if ((typeof data[key] !== "number")
         && (typeof data[key] !== "string")
         && (typeof data[key] !== "boolean")
         && (typeof data[key] !== "object")
         && (data[key] !== null)) {
            data[key] = null;
        }
        if (data[key] !== null) {
            if ((typeof data[key] === "string") && (Number.isInteger(parseInt(data[key], 10)))) {
                data[key] = parseInt(data[key], 10);
            } else if (typeof data[key] === "string") {
                data[key] = data[key].
                    replace(/&/gu, "&amp;").
                    replace(/"/gu, "&quot;").
                    replace(/'/gu, "&#039;").
                    replace(/</gu, "&lt;").
                    replace(/>/gu, "&gt;");
            } else if (typeof data[key] === "object") {
                data[key] = parseMessage(JSON.stringify(data[key]));
            }
        }
    });
    return data;
}

wss.on("connection", (ws) => {
    ws.on("message", (data) => {
        data = parseMessage(data);
        switch (data.method) {
            case "player_create":
                DemineurPlayer.createPlayer(d, ws, data.name, data.room, data.mode);
                break;
            case "grid_style":
                DemineurGrid.styleGrid(d, ws, data.gridStyle);
                break;
            case "grid_create":
                DemineurGrid.checkGrid(d, ws, data.rowsTotal, data.colsTotal, data.bombsTotal, data.botsTotal, data.timeBeforeStart, data.gridStyle, data.gameMode, data.startingCell);
                break;
            case "grid_click":
                DemineurGrid.clickGrid(d, ws, data.row, data.col);
                break;
            case "grid_flag":
                DemineurGrid.flagGrid(d, ws, data.row, data.col);
                break;
            case "battle_royale_strategy":
                DemineurPlayer.battleRoyaleSetStrategy(d, ws.uuid, data.battleRoyaleStrategy);
                break;
            default:
                break;
        }
    });
    ws.on("close", () => {
        if (ws.uuid) {
            DemineurPlayer.deletePlayer(d, ws.uuid);
        }
    });
    ws.on("error", () => {
        if (ws.uuid) {
            DemineurPlayer.deletePlayer(d, ws.uuid);
        }
    });
});
