# Démineur Multijoueur

Un Démineur Multijoueur crée en Node.js avec [ws](https://www.npmjs.com/package/ws), avec modes de jeux versus, co-op et battle royale, gestion du HTTPS, et styles personnalisables.
Le jeu est fonctionnel et testable en ligne à cette adresse : https://www.desmu.fr/demineur/

## Installation

### Paquets Node.js

- Assurez-vous de disposer d'une version de [Node.js](https://nodejs.org/fr/) sur votre serveur.
- Exécutez `npm install --prefix ./server ws` à la racine du projet pour installer les dépendances listées dans le fichier `package.json`.

### Client

Modifiez le contenu du fichier `client/configuration.json` en fonction de votre environnement.
- Si vous souhaitez tester le démineur sur votre ordinateur, le contenu du fichier sera le suivant (changez les variables `serverDomain` et `serverPort` au besoin):
```
{
  "env": "dev",
  "serverDomain": "localhost",
  "serverPort": 8081
}
```
- Si vous souhaitez mettre en place le démineur sur un serveur, le contenu du fichier sera le suivant (changez la variable `serverPort` si elle est déjà utilisée par un autre programme, et la variable `serverDomain` pour la faire correspondre à votre nom de domaine) :
```
{
  "env": "prod",
  "serverDomain": "www.domain.tld",
  "serverPort": 8081
}
```

### Serveur

Modifiez le contenu du fichier `server/configuration.json` en fonction de votre environnement.
- Si vous souhaitez tester le démineur sur votre ordinateur, le contenu du fichier sera le suivant (changez la variable `serverPort` au besoin):
```
{
  "env": "dev",
  "certPem": "",
  "keyPem": "",
  "serverPort": 8081
}
```
- Si vous souhaitez mettre en place le démineur sur un serveur, le contenu du fichier sera le suivant (changez la variable `serverPort` si elle est déjà utilisée par un autre programme, et les variables `certPem` et `keyPem` pour les faire correspondre aux fichiers .pem associés à votre nom de domaine) :
```
{
  "env": "dev",
  "certPem": "/etc/letsencrypt/live/www.domain.tld/fullchain.pem",
  "keyPem": "/etc/letsencrypt/live/www.domain.tld/privkey.pem",
  "serverPort": 8081
}
```
- Pour une installation sur un serveur, n'oubliez pas d'ouvrir le port défini dans la configuration.

### Génération des fichiers

- Une fois la configuration prête, exécutez `node build.js` à la racine du projet pour générer les dossiers `build/client` et `build/server`.
- Accédez au client depuis le fichier `build/client/index.html`.
- Démarrez le serveur en exécutant `node build/server/index.js`.

## Styles

Le jeu est fourni avec le style de base du démineur de Windows XP, ainsi qu'une version agrandie de ce même style. Il est possible d'ajouter d'autres styles en dupliquant un des dossiers disponibles dans le répertoire `client/styles` et en personnalisant les images et le fichier `customstyle.css` du dossier dupliqué. Une fois le style prêt, il faudra l'ajouter dans le fichier `index.html` en option du `select#grid_style`, avec comme valeur le nom du dossier dupliqué.

## Règles du jeu

L'application comporte deux formulaires pour paramétrer la partie :
- Le premier définit le type de salle de jeu, il est nécessaire de recharger la page pour changer de salle.
- Le deuxième définit le type de partie, il est possible d'en changer entre chaque partie (le créateur de la salle doit cliquer sur "Générer") pour lancer une partie.

### Types de salles

- **Publique** : Le code de salle reste visible pour être transmis plus facilement, les joueurs peuvent rejoindre la partie par hasard sans avoir le code.
- **Privée** : Le code de salle est affiché brièvement pour restreindre son accès, seuls les joueurs disposant du code peuvent rejoindre la partie.

### Types de parties

#### Modes de jeu

- **Battle Royale** : Les joueurs résolvent plusieurs grilles à la suite et envoient des mines sur les grilles de leurs adversaires.
- **Coop** : Les joueurs résolvent une unique grille ensemble.
- **Versus** : Les joueurs résolvent la même grille séparément.

#### Autres paramètres

- **Nombre de lignes / colonnes / bombes / bots** : Niveau de difficulté paramétrable. Maximum 50 lignes, 50 colonnes, (nombre de lignes * nombre de colonnes) bombes et 99 bots.
- **Secondes avant le lancement de la partie** : Temps d'attente additionnel enclenché au clic sur le bouton "Générer" pour permettre aux joueurs de se préparer avant la partie.
- **Première case révélée** : Lance un clic aléatoire sur une case sans bombe en début de partie pour révéler une partie de la grille. Option activable si le nombre de bombes présentes sur la grille n'excède pas 50% du total de cases.
- **Style** : Thème visuel de la grille.

#### Fonctionnement du Battle Royale

Dans ce mode, les joueurs reçoivent chacun une suite de grilles à compléter. Compléter correctement une grille diminue le taux d'apparition de bombes de sa grille suivante (avec un minimum de 5%), et augmente le taux d'apparition des bombes dans les grilles des adversaires (avec un maximum de 33%). Perdre une grille augmente le taux d'apparition de bombes de sa grille suivante. Un temps de pause de trois secondes est accordé entre chaque grille.

Une grille est considérée comme perdue si le joueur clique sur une bombe ou met plus de dix secondes à cliquer sur une case (placer un drapeau ne compte pas). Si le taux d'apparition des bombes dépasse 33%, l& partie est terminée pour le joueur. Le dernier joueur en lice est considéré gagnant.

Une attaque consiste à augmenter le taux d'apparition de bombes sur la prochaine grille du joueur attaqué. Le dernier joueur à avoir attaqué un autre joueur dont la partie se termine voit son compteur de K.O. augmenter de un. Il remporte également un fragment de chevron, plus le nombre de fragments de chevrons dont disposait le joueur perdant. Le nombre de chevrons possédé donne un bonus de puissance aux futures attaques, augmentant davantage le taux d'apparition de bombes. Le premier chevron s'obtient après avoir récupéré deux fragments, le second après six, le troisième après quatorze, le dernier après trente.

Quatre modes d'attaque sont proposés au joueur réussissant une grille :
- **Aléatoire** : attaque un joueur au hasard.
- **K.O. faciles** : attaque le joueur au taux d'apparition de bombes le plus élevé.
- **Chevrons** : attaque le joueur au nombre de chevrons le plus élevé.
- **Riposte** : attaque les joueurs ayant précédemment attaqué le joueur lançant l'attaque.
