import fs from "fs";
import path from "path";
import {exec, spawn} from "child_process";

/**
 * Copie d'un fichier. Merci StackOverflow https://stackoverflow.com/a/26038979/12634214 .
 *
 * @param {string} source Chemin du fichier à copier.
 * @param {string} target Chemin du fichier copié.
 */
function copyFileSync (source, target) {
    let targetFile = target;
    if (fs.existsSync(target)) {
        if (fs.lstatSync(target).isDirectory()) {
            targetFile = path.join(target, path.basename(source));
        }
    }
    fs.writeFileSync(targetFile, fs.readFileSync(source));
}

/**
 * Copie d'un dossier. Merci StackOverflow https://stackoverflow.com/a/26038979/12634214 .
 *
 * @param {string} source Chemin du dossier à copier.
 * @param {string} target Chemin du dossier copié.
 */
function copyFolderRecursiveSync (source, target) {
    let files = [];
    const targetFolder = path.join(target, path.basename(source));
    if (targetFolder.indexOf("node_modules") === -1) {
        if (!fs.existsSync(targetFolder)) {
            fs.mkdirSync(targetFolder);
        }
        if (fs.lstatSync(source).isDirectory()) {
            files = fs.readdirSync(source);
            files.forEach((file) => {
                const curSource = path.join(source, file);
                if (fs.lstatSync(curSource).isDirectory()) {
                    copyFolderRecursiveSync(curSource, targetFolder);
                } else {
                    copyFileSync(curSource, targetFolder);
                }
            });
        }
    }
}

/**
 * Minification des fichiers d'un dossier.
 *
 * @param {string} folder Chemin du dossier à minifier.
 * @param {Array} ignoredPaths Tableau des noms de fichiers à ne pas minifier.
 * @param {Array} filesToRemove Tableau des noms de fichiers à supprimer.
 */
function minify (folder, ignoredPaths, filesToRemove) {
    if (fs.lstatSync(folder).isDirectory()) {
        const files = fs.readdirSync(folder);
        files.forEach((file) => {
            const curSource = path.join(folder, file);
            if (fs.lstatSync(curSource).isDirectory()) {
                minify(curSource, ignoredPaths, filesToRemove);
            } else {
                const extensions = [".css", ".html", ".js", ".json"];
                if (filesToRemove.indexOf(file) !== -1) {
                    fs.unlinkSync(curSource);
                } else if (extensions.indexOf(path.extname(curSource)) !== -1) {
                    fs.readFile(curSource, "utf-8", (er, data) => {
                        if (er) {
                            throw er;
                        }
                        let dataMinified = data;
                        const isIgnored = ignoredPaths.some((ignoredPath) => {
                            if (curSource.indexOf(ignoredPath) !== -1) {
                                return true;
                            }
                            return false;
                        });
                        if (!isIgnored) {
                            dataMinified = dataMinified.replace(/\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*|<!--[\s\S]*?-->$/gmu, "");
                            dataMinified = dataMinified.replace(/\r?\n|\r/gu, " ");
                            dataMinified = dataMinified.replace(/ {2}/gu, "");
                        }
                        fs.writeFile(curSource, dataMinified, "utf-8", (err) => {
                            if (err) {
                                throw err;
                            }
                        });
                    });
                }
            }
        });
    }
}

if (fs.existsSync("./build")) {
    fs.rmSync("./build", {"recursive": true});
}
fs.mkdirSync("./build", {"recursive": true});
copyFolderRecursiveSync("./client", "./build");
copyFolderRecursiveSync("./server", "./build");

minify("./build", [], []);

fs.mkdirSync("./build/server/node_modules", {"recursive": true});
spawn("npm", ["install", "--prefix", "./build/server", "ws"], {"stdio": "inherit"});


